public class BigRoot {
    private Squarethree st;

    public BigRoot(Squarethree st)
    {
        this.st = st;
    }

    public double getMaxRoot() {
        if (st.getRoots() == null) {
            throw new IllegalArgumentException();
        } else {
            return (st.getRoots()[0] > st.getRoots()[1]) ? st.getRoots()[0] : st.getRoots()[1];
        }
    }
}