import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class testBigRoot {
    @Test(expected = Exception.class)
    public void testDLowerThanZeroOrAIsZero() {
        Squarethree st = new Squarethree(0, 1, 1);
        BigRoot br = new BigRoot(st);
        br.getMaxRoot();
    }

    @Test
    public void testDEqualsZero() {
        Squarethree st = new Squarethree(1, 2, 1); //ura
        BigRoot br = new BigRoot(st);
        assertEquals(br.getMaxRoot(), -1, 10E-9);
    }

    @Test
    public void testDBiggerThanZero() {
        Squarethree st = new Squarethree(1, 2, -3);
        BigRoot br = new BigRoot(st);
        assertEquals(br.getMaxRoot(), 1, 10E-9);
    }

}
